import React from 'react';

const TotalOrder = props => {
    const {available, setAvailable} = props;

    const result = available.filter((element) => element.count > 0);

    const getRemove = id => {
        const index = available.findIndex(i => i.id === id);
        const availableCopy = [...available];
        availableCopy[index].count = 0;
        setAvailable(availableCopy);

    }
    const getTotalPrice = () => {
        return available.reduce((acc, available) => {
            if (available.count > 0) {
                return acc + (available.price * available.count);
            }

            return acc;

        }, 0)
    };

    return (
        <div>
            {result.map((element) => {
                const {id, name, price, count} = element;

                return (
                    <div key={element}>
                        <div>
                            {name} - {price} count - {count} = {count * price}{" "}
                        </div>
                        <div>
                            <button className="btn-remove" onClick={() => getRemove(id)}>Remove</button>
                        </div>
                    </div>
                );
            })}

            <div className="total-txt">Total Price : {getTotalPrice()}</div>
        </div>
    );
};

export default TotalOrder;