import React from 'react';

const AvailableProducts = props => {
    const {available, setAvailable} = props;

    const handleClick = index => {
        const availableCopy = [...available];
        availableCopy[index].count += 1;
        setAvailable(availableCopy);
    };

    return (
        <div>
            {available.map((elements, index) => {
                const {id, name, price, count} = elements;
                return (
                    <div className="all-product" key={id} onClick={() => handleClick(index)}>
                        <div className="name">{name}</div>
                        <div className="price">{price}</div>
                        <div className="price">{count}</div>
                    </div>
                );
            })}
        </div>
    );
};

export default AvailableProducts;