import React, {useState} from "react";
import "./App.css";
import AvailableProducts from "../components/AvailableProducts/AvailableProducts";
import TotalOrder from "../components/TotalOrder/TotalOrder";

const App = () => {
    const [available, setAvailable] = useState([
        {id: 1, name: "Hamburger", price: 80, count: 0},
        {id: 2, name: "Cheeseburger", price: 90, count: 0},
        {id: 3, name: "Fries", price: 45, count: 0},
        {id: 4, name: "Coffee", price: 70, count: 0},
        {id: 5, name: "Tea", price: 50, count: 0},
        {id: 6, name: "Coca-Cola", price: 40, count: 0}
    ]);

    return (
        <div className="App">
            <TotalOrder available={available} setAvailable={(v) => setAvailable(v)}/>
            <AvailableProducts available={available} setAvailable={(v) => setAvailable(v)}/>
        </div>
    );

}

export default App;

